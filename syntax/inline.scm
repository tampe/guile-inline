(define-module (syntax inline)
  #:use-module (syntax not-inline)
  #:export (inline)
  #:replace (define define*))

(define-syntax inline
  (syntax-rules () ((_ x) x)))

(define-syntax-parameter level-not-inline
  (syntax-rules ()
    ((_ f x)
     (let ((r (not-inline x)))
       (when (and (procedure? r)
                  (not (procedure-name r)))
         (set-procedure-property! r 'name 'f))
       r))))

(define-syntax n-i
  (syntax-rules (inline)
    ((_ f (inline x))
     (syntax-parameterize ((level-not-inline
                            (syntax-rules () ((_ y) y)))) x))
    ((_ f x)
     (level-not-inline f
      (syntax-parameterize ((level-not-inline
                             (syntax-rules () ((_ y) y)))) x)))))

(define-syntax define
  (syntax-rules ()
    ((_ (f . x) . code) ((@ (guile) define) f (n-i f  (lambda x . code))))
    ((_ f         code) ((@ (guile) define) f (n-i f  code)))))

(define-syntax define*
  (syntax-rules ()
    ((_ (f . x) . code) ((@ (guile) define) f (n-i f  (lambda* x . code))))))
  
