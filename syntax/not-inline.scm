(define-module (syntax not-inline)
  #:export (not-inline))

(cond-expand
  (guile-3.0
   (define not-inline (lambda (x) x)))
  ((or guile-1.8 guile-2.0 guile 2.2)
   (define-syntax not-inline
     (syntax-rules ()
       ((_ x) x)))))
