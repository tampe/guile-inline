# guile-inline

copyright : Stefan Israelsson Tampe
LICENSE   : LGPL v2 or higher

Simple repo to ease the handling of inlinability in guile-3.0 for guile-2.2 an dlower it does not have an effect.

These modules solves an issue with too agressive inlining meaning that exported symbols can't change the behavior of the module by setting it to another value.

Source this directory

The simplest solution is to use

(use-modules syntax not-inline)

And then guard values that should not be inlined like
(define f (not-inline 12))

Then the compiler can't deduce the value of f as it needs to be calculated at
load time preventing it to replace f with 12 inside procedures and code.

Another option is to use define and define* together with the inline symbol so that all normal uses of define and define* is per default using not-inline and to enable inlining one need to guard it like

(use-modules (syntax inline))
(define f (inline 12))

The system is intelligent enough to not prevent inlining with local defines inside defines. This is not perfect as it will prevent inlining in global let environments, this is probably not catastrofic as it would not change the semantics fo the code.
